import { Component, OnInit } from '@angular/core';
import { ProductlistService } from '../productlist.service';
import { UserserviceService } from '../user/userservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
products:any;
username:string;
  productlist:any=[];
  vegetables:any=[];
  leafyvegetables:any=[];
  fruits:any=[];
  statusveg:boolean=false;
  statusleaf:boolean=false;
  statusfruits:boolean=false;
  productliststatus:boolean=true;
  searchterm:string;
  item:any;
  productcart:any;
  homeproducts:any;
  constructor(private pl:ProductlistService,private ul:UserserviceService,private router:Router) { }

  ngOnInit() {
    this.pl.getuserproducts().subscribe((res)=>{
     this.products=res["message"];
     console.log("products are",this.products);
    })
  }
  data:any;
  cart:any=[];
  addtocart(i)
  {
    // i.username=this.username;
    // console.log("cart username is",this.ul.username)
    // this.ul.cartData(i).subscribe((res)=>{
    //   if(res["message"]=="product added"){
    //     alert("product added to your cart");
    //  this.productcart=res["cart"]
    //  this.productcart.totalprice=this.ul.totalprice;
    //  console.log("cart price total",this.productcart.totalprice)
    //     }

    //     })
    this.cart.push(i);
    console.log(this.cart);
    localStorage.setItem('cartobj',JSON.stringify(this.cart));
    this.data=localStorage.getItem('cartobj');
    console.log('cartdata',JSON.parse(this.data));
      this.ul.cartData(this.data).subscribe((res)=>{
       this.homeproducts=res["cartobj"];

    })
      }
      sendveg()
      {
      for(let v of this.products)
      {
        console.log(v.category)
        if(v.category=="vegetables")
        {
          this.vegetables.push(v)
          console.log(this.vegetables)
          this.statusveg=true;
          this.productliststatus=false;
          this.statusleaf=false;
          this.statusfruits=false;
        }
      }
    } 
    sendleafveggies()
    {
      for(let v of this.products)
      {
        console.log(v.category)
        if(v.category=="leafy veggies")
        {
          this.leafyvegetables.push(v)
          console.log(this.leafyvegetables)
          this.statusleaf=true;
          this.productliststatus=false;
          this.statusfruits=false;
          this.statusveg=false;
        }
      }
    }
    sendfruits()
{
  for(let v of this.products)
  {
    console.log(v.category)
    if(v.category=="fruits")
    {
      this.fruits.push(v)
      console.log(this.fruits)
      this.statusfruits=true;
      this.productliststatus=false;
      this.statusveg=false;
      this.statusleaf=false;

    }
  }
} 
// addtocart(i)
// {

// }
basket()
{
this.router.navigate(['./homeproducts']);
}

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private hc:HttpClient) { }
  doRegister(userobj):Observable<object>
  {
    return this.hc.post('/user/register',userobj);
  }
  search(data):Observable<any>{
    return this.hc.get('/user/readuser/:email',data)
      }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { EditproductComponent } from './editproduct/editproduct.component';
import { TotalordersComponent } from './totalorders/totalorders.component';
import { ViewComponent } from './view/view.component';


const routes: Routes = [{path:"admindashboard",component:AdmindashboardComponent},
                        {path:"productlist",component:ProductlistComponent},
                        {path:"editproduct",component:EditproductComponent},
                        {path:"totalorders",component:TotalordersComponent},
                        {path:"view",component:ViewComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { UserserviceService } from 'src/app/user/userservice.service';
import { ProductlistService } from 'src/app/productlist.service';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-totalorders',
  templateUrl: './totalorders.component.html',
  styleUrls: ['./totalorders.component.css']
})
export class TotalordersComponent implements OnInit {
totalorders:any;
totalprice:any;
  constructor(private ul:UserserviceService,private router:Router) { }

  ngOnInit(){
    
this.ul.totalorders().subscribe((res)=>
{
  if(res["message"]=="totalorders")
  {
   this.totalorders=res["totalorders"];
   console.log("totalorders",this.totalorders);
  }
})
  }
view(i)
{
  this.ul.orders=i;
  this.router.navigate(['./view'])
}
}

import { Component, OnInit } from '@angular/core';
import { ProductlistService } from 'src/app/productlist.service';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/user/userservice.service';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent implements OnInit {

  constructor(private pl:ProductlistService,private router:Router,private ul:UserserviceService) { }
  // getData(data)
  // {

  // }
   
product:any=[];
productlist:any[];
editname:any;
searchterm:string;
username:any;
  ngOnInit() {
  
    this.pl.getproduct().subscribe((res)=>{
      // this.productlist=res;

      if(res["message"]=="no product")
      {
        alert("product not available")
      }
      else{
        this.product=res["message"];
        console.log(this.product);
        // this.username=this.ul.cartusername;
      }

    })
  }
  deleteObj:any;
  pname:any;
  d:boolean;
deletedata(i)
{
  console.log(i);
  // this.deleteObj=i;
  // console.log("delete obj is",this.deleteObj);
    
  // this.pname=this.deleteObj.name;
  this.d=confirm("do you want to delete")
  if(this.d==true){
this.pl.delete(i.name).subscribe((res)=>{
  
    if(res["message"]=="no product")
  {
    alert("no product to delete")
  }
  if(res["message"]="product deleted")
  {
    alert("product is deleted")
    this.ngOnInit();
  }
})
  }
  else this.ngOnInit();

}
update(i)
{
  this.pl.editdata=i;
this.router.navigate(['./editproduct'])

}
obj:any;
totalorders(obj)
{
  this.router.navigate(['./totalorders'])
}
// obj:any;
// update(product)
// {
//   this.obj=product;
//   console.log(this.obj);
// }
}

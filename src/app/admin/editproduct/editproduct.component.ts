import { Component, OnInit } from '@angular/core';
import { ProductlistService } from 'src/app/productlist.service';
import { observable } from 'rxjs';

@Component({
  selector: 'app-editproduct',
  templateUrl: './editproduct.component.html',
  styleUrls: ['./editproduct.component.css']
})
export class EditproductComponent implements OnInit {
  obj:any=[];
 editname:any;
   constructor(private pl:ProductlistService) { }
   ngOnInit(): void {
    this.editname=this.pl.editdata.name
    console.log(this.editname);
    this.pl.getdata(this.editname).subscribe((data)=>{
      if(data["message"]=="no product"){
        alert("no product available")
      }
      else{
       this.obj=data["message"];
       
      }
    })

  }

   
  editproduct(product)
  {
// this.obj=this.obj.push(product);
// console.log(this.obj);
product.userquantity=0.5
product.cartprice=product.price;

this.pl.edit(product).subscribe((res)=>{
  if(res["message"]=="no product")
  {
    alert("product is not existed")
  }
  if(res["message"]=="product updated")
  {
    alert("product data updated")
  }
})
  }
//   obj2:any;
// update(data)
// {
//   this.obj2=data;
//   console.log(this.obj2);
// }

}

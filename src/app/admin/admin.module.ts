import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { ProductlistComponent } from './productlist/productlist.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { EditproductComponent } from './editproduct/editproduct.component';
import { SearchPipe } from './search.pipe';
import { TotalordersComponent } from './totalorders/totalorders.component';
import { ViewComponent } from './view/view.component'
// import { SearchPipe } from '../search.pipe';

@NgModule({
  declarations: [AdmindashboardComponent, ProductlistComponent, EditproductComponent, SearchPipe, TotalordersComponent, ViewComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
   
  ]
})
export class AdminModule { }

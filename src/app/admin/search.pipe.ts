import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(product:object[],searchterm:string):any {
    if(searchterm==undefined)
    {
      return product;
    }
    else{
      return product.filter(x=>x["name"].toLowerCase().indexOf(searchterm.toLowerCase())!==-1)
    }
  }

}

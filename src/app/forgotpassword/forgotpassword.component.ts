import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  constructor(private ls:LoginService) { }
  data:any=[];
   otp:number;
  forgot(data)
  {
      this.ls.search(data).subscribe((response)=>{
        
      if(response["message"]=="no email")
      {
        alert("please enter a valid email")
      }
     else{
      this.otp=response["otp"]
      console.log("otp is",this.otp)
    }
    })
    

  }


  ngOnInit(): void {
  }

}

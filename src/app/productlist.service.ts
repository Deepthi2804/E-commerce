import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductlistService {
name:any;
editdata:any;
username:any;
homeproducts:any=[];
// productcartusername:any;
  constructor(private hc:HttpClient) { }
  product(product):Observable<any>
  {
    return this.hc.post('/admin/product',product);
  }
  
  getproduct():Observable<any>
  {
    return this.hc.get('/admin/productdata')
  }
  edit(product):Observable<any>
  {
    return this.hc.put('/admin/updateproduct',product)
  }
  delete(name):Observable<any>
  {
    return this.hc.delete(`/admin/deleteproduct/${name}`)
  }
  getdata(editname):Observable<any>
  {
    return this.hc.get(`/admin/editdataproduct/${editname}`)
  }
  getuserproducts():Observable<any>
  {
    return this.hc.get('/admin/userproductdata')
  }
  // totalorders():Observable<any>
  // {
  //   return this.hc.get('/ship/totalorders')
  // }
}

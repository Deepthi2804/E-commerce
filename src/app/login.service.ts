import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  userstatus:boolean=false;
username:string;
  constructor(private hc:HttpClient) { }
  cartusername:string;
  doLogin(data):Observable<any>
  {
   return this.hc.post('/user/login',data);
     }
     //implementation of logout
     
     doLogout()
     {
       localStorage.removeItem("token");
       this.userstatus=false;
     }
     search(data):Observable<any>{
      return this.hc.get('/user/readuser/:email',data)
        }
        
  }


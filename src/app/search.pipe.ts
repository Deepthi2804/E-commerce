import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(products:object[],searchterm:string):any {
    if(searchterm==undefined)
    {
      return products;
    }
    else{
      return products.filter(x=>x["name"].toLowerCase().indexOf(searchterm.toLowerCase())!==-1)
    }
  }
  }



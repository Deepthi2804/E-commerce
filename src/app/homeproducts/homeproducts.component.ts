import { Component, OnInit } from '@angular/core';
import { ProductlistService } from '../productlist.service';
import { UserserviceService } from '../user/userservice.service';

@Component({
  selector: 'app-homeproducts',
  templateUrl: './homeproducts.component.html',
  styleUrls: ['./homeproducts.component.css']
})
export class HomeproductsComponent implements OnInit {
homeproducts:any=[];
totalprice:number;
cartprice:number;
deletedproduct:string;
  constructor(private pl:ProductlistService,private ul:UserserviceService) { }

  ngOnInit() {
    this.totalprice=0;
  this.cartprice=0;
    this.homeproducts=JSON.parse(localStorage.getItem('cartobj'));
    // this.homeproducts=this.pl.homeproducts;
    // this.ul.cartData(this.homeproducts).subscribe((res)=>{
    //          this.homeproducts=res["cartobj"];
    //         }) 
    console.log("home products are",this.homeproducts);
    for(let a of this.homeproducts)
       {
         this.totalprice=this.totalprice+a.cartprice;
         this.ul.totalprice=this.totalprice;
          this.deletedproduct=this.homeproducts.name
        }
         
  }
  decrease(v)
  {
    if(v.userquantity==0){
      v.quantity=v.quantity+0.5;
      for(var i=0;i<=this.homeproducts.length,i++;)
      {
      var myKitten = localStorage.get('cartobj').filter(function(cartobj){
         cartobj.name == this.homeproducts.name;
         cartobj.cartprice=this.homeproducts.cartprice;
         cartobj.totalprice=this.homeproducts.totalprice;
     })[i];
     localStorage.setItem('mykittens',myKitten);
     console.log("products",localStorage.getItem('mykitten'));
    }
      //edit the product data
    //   this.ul.updatecart(v).subscribe((res)=>{
    //     res["message"];
    //     this.ngOnInit();
     
    //   this.ul.deleteitem(v).subscribe((res)=>{
    //     if(res["message"]=="product deleted")
    //     alert(res["message"]);
    //     this.ngOnInit();
    //   })
    // })
    }
    else{
      v.quantity=v.quantity+0.5;
      
      // this.pl.edit(v).subscribe((res)=>{
      //   console.log("product edited",res["message"])
      //   v.userquantity=v.userquantity-0.5;
      //   v.cartprice=v.cartprice-v.price;
      //   this.ul.updatecart(v).subscribe((res)=>{
      //     res["message"];
      //     this.ngOnInit();
      //   })
      // })
    }
  
  }
  increase(v)
  {
  if(v.userquantity>=v.quantity){
    alert("limited stock");
    this.ngOnInit();
  }
  else{
    v.quantity=v.quantity-0.5;
    
    this.pl.edit(v).subscribe((res)=>{
      
      console.log("product edited",res["message"])
      v.userquantity=v.userquantity+0.5;
    v.cartprice=v.cartprice+v.price;
        // this.ngOnInit();
   
       this.ul.updatecart(v).subscribe((res)=>{
        res["message"];
        this.ngOnInit();
     
    })
  })
  }
  }
  username:any;
  
  deletecart(){
    this.username=this.ul.username;
  this.ul.deletecart(this.username).subscribe((res)=>{
      if(res["message"]=="cart deleted"){
        alert("cart items deleted")
        this.ngOnInit();
      // res["result"]
       }
    
  })
  }
}

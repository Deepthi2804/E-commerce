import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements HttpInterceptor{
  intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>>
  {
    //get token from localstorage
    var signedToken=localStorage.getItem("token");
    // if token is there add the token to the header of req object
    if(signedToken)
    {
      var clonedReqObj=req.clone({headers:req.headers.set("Authorization","Bearer "+signedToken)})
      console.log("cloned obj is",clonedReqObj)
      return next.handle(clonedReqObj)
    }
    else{
      return next.handle(req);
    }
    
  }
  

  
}

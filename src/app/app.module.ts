import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {UserModule} from './user/user.module';
import {AdminModule} from './admin/admin.module';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component'
import { AuthorizationService } from './authorization.service';
import { SearchPipe } from './search.pipe';
import { DatePipe } from '@angular/common';
import { HomeproductsComponent } from './homeproducts/homeproducts.component';
// import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ForgotpasswordComponent,
    SearchPipe,
    HomeproductsComponent  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    UserModule,
    AdminModule
  ],
  providers: [DatePipe,{provide:HTTP_INTERCEPTORS,
    useClass:AuthorizationService,
  multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
data:any;
  constructor(private rs:RegisterService,private router:Router) { }
  sendData(userobj)
  {
    userobj.role="user"
    console.log(userobj);
    this.rs.doRegister(userobj).subscribe((response)=>{
      //console.log(res["message"]);
      if(response["message"]=="user created")
      {
        alert("user created successfully");
        this.router.navigate(['./login'])
        
        console.log("data from register",this.data);
      }
      if(response["message"]=="user already existed")
      {
        alert("user already existed");
      }

    })
  }
  
  ngOnInit() {
   
    }

}

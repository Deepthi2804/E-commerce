import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
username:string;
deletedproduct:string;
cartdata:any;
orders:any={};
totalprice:any;
today:any;

  constructor(private hc:HttpClient) { }
  // cartData(data):Observable<any>
  // {
  //   console.log(data)
  //   return this.hc.post(`/cart/cartdata/${data.name}/${this.username}`,data)
  // }
  cartData(data):Observable<any>
  {
    console.log(data)
    return this.hc.post('/cart/cartdata',data)
  }
  getcartdata():Observable<any>
  {
    return this.hc.get(`/cart/getcartdata/${this.username}`)
  }
  deleteitem(v):Observable<any>
  {
    return this.hc.delete(`/cart/deleteitem/${v.name}`);
  }
  updatecart(v):Observable<any>
  {
    return this.hc.put('/cart/updatecart',v);
  }
  deletecart(username):Observable<any>
  {
    return this.hc.delete(`/cart/deletecart/${this.username}`);
  }
  postorder(obj):Observable<any>
  {
    return this.hc.post('/order/orderdata',obj);
  }
  getaddress(username):Observable<any>
  {
    return this.hc.get(`/order/getaddress/${this.username}`);
  }
  postaddress(obj):Observable<any>
  {
    console.log(obj)
    return this.hc.post('/ship/postaddress',obj);
  }
  // putaddress(obj):Observable<any>
  // {
  //   return this.hc.put(`/cart/address/${this.username}`,obj);
  // }
  //remove address
  deleteaddress(obj):Observable<any>
  {
    return this.hc.delete(`/order/deleteaddress/${this.username}`,obj);
  }
  totalorders():Observable<any>
  {
    return this.hc.get('/ship/totalorders')
  }
  userorders():Observable<any>
  {
    return this.hc.get(`/ship/userorders/${this.today}`);
  }
}

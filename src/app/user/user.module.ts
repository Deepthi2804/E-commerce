import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { AllcategoriesComponent } from './allcategories/allcategories.component';
import { VegetablesComponent } from './vegetables/vegetables.component';
import { LeafyveggiesComponent } from './leafyveggies/leafyveggies.component';
import { FruitsComponent } from './fruits/fruits.component';
import { SearchproductPipe } from './searchproduct.pipe';
import {FormsModule} from '@angular/forms';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';
import { OrdersComponent } from './orders/orders.component';
import { ShippingComponent } from './shipping/shipping.component';
import { UserordersComponent } from './userorders/userorders.component'
// import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [UserdashboardComponent, 
    AllcategoriesComponent,
     VegetablesComponent, 
     LeafyveggiesComponent, 
     FruitsComponent,
      SearchproductPipe,
       ShoppingcartComponent,
        OrdersComponent, 
        ShippingComponent, UserordersComponent,
        ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule
  ]
})
export class UserModule { }

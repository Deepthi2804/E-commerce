import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
// import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit {
  getaddress:any=[];
  //cartdata:any=[];
  totalprice:any;
  constructor(private ul:UserserviceService,private datepipe:DatePipe,private router:Router) { }

  ngOnInit() {
    this.ul.getaddress(this.ul.username).subscribe((res)=>{
      if(res["message"]=="address")
      {
        this.getaddress=res["addr"];
        console.log("address obj is",this.getaddress);
        // this.ngOnInit();
      }
     

    })
    
  }
  order(obj)
  {
    obj.username=this.ul.username;
    obj.totalprice=this.ul.totalprice;
    console.log("total price is",obj.totalprice);
    console.log(obj.username);
  this.ul.postorder(obj).subscribe((res)=>{
      if(res["message"]=="address added"){
      alert("Address added");
      res["address"];
      this.ngOnInit();
    }
  })
  }
  obj2:any={};
  today:any=Date.now();
  obj1:any;
  deliver(obj1)
  {
    
    console.log("cartdata of items",this.ul.cartdata);
    console.log("object is",obj1);
    this.obj2.username=this.ul.username;
    this.obj2.address=obj1;
    this.obj2.address.payment="cash on delivery"
    // this.obj2.cartdata.totalprice=this.ul.totalprice;
    //get today date from datepipe
    this.today=this.datepipe.transform(new Date(),"short");
    console.log("today date is",this.today);
    this.obj2.date=this.today;
    this.ul.today=this.today;
    
    
    
    
 this.obj2.cartdata=this.ul.cartdata;
 this.obj2.totalprice=this.ul.totalprice;
 this.obj2.cartdata.totalprice=this.ul.totalprice;
 console.log("final total price",this.obj2.cartdata.totalprice);
 
 //this.cartdata.address=obj1;
// console.log("after adding address",this.cartdata)
 this.ul.postaddress(this.obj2).subscribe((res)=>{
     res["message"];
     this.router.navigate(['./userorders']);
  })

 
  }
  obj:any;
  edit(i)
  {
    console.log(i);
        this.obj=i;
        console.log(this.obj);
  }
  remove(i)
  {
    this.ul.deleteaddress(i).subscribe((res)=>{
      if(res["message"]=="address deleted")
      {
        alert("address deleted");
      }
      this.ngOnInit();
    })
    
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeafyveggiesComponent } from './leafyveggies.component';

describe('LeafyveggiesComponent', () => {
  let component: LeafyveggiesComponent;
  let fixture: ComponentFixture<LeafyveggiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeafyveggiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeafyveggiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { AllcategoriesComponent } from './allcategories/allcategories.component';
import { VegetablesComponent } from './vegetables/vegetables.component';
import { LeafyveggiesComponent } from './leafyveggies/leafyveggies.component';
import { FruitsComponent } from './fruits/fruits.component';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';
import { OrdersComponent } from './orders/orders.component';
import { UserordersComponent } from './userorders/userorders.component';


const routes: Routes = [{path:"userdashboard/:username",component:UserdashboardComponent},
                        {path:"shoppingcart",component:ShoppingcartComponent},{path:"orders",component:OrdersComponent},
                        {path:"userorders",component:UserordersComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-userorders',
  templateUrl: './userorders.component.html',
  styleUrls: ['./userorders.component.css']
})
export class UserordersComponent implements OnInit {
userorders:any;
  constructor(private ul:UserserviceService) { }

  ngOnInit() {
this.ul.userorders().subscribe((res)=>{
  if(res["message"]=="your orders")
  {
    this.userorders=res["userorders"];
    console.log("userorders",this.userorders);
  }
})
  }

}

import { Component, OnInit } from '@angular/core';
import { ProductlistService } from 'src/app/productlist.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { stringify } from 'querystring';
import { UserserviceService } from '../userservice.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-userdashboard',
  templateUrl: './userdashboard.component.html',
  styleUrls: ['./userdashboard.component.css']
})
export class UserdashboardComponent implements OnInit {

  constructor(private pl:ProductlistService,private ul:UserserviceService,private ar:ActivatedRoute,private router:Router) { }
  username:string;
  productlist:any=[];
  vegetables:any=[];
  leafyvegetables:any=[];
  fruits:any=[];
  statusveg:boolean=false;
  statusleaf:boolean=false;
  statusfruits:boolean=false;
  productliststatus:boolean=true;
  searchterm:string;
  item:any;
  productcart:any;
  totalprice:any;
// productcartusername:any;
userquantity:number;

  //adding products to cart
  addtocart(i)
  {
    i.username=this.username;
    
    // this.ul.username=i.username;
    // i.userquantity=0.5;
    // i.cartprice=i.price;
    console.log("cart username is",this.ul.username)
    this.ul.cartData(i).subscribe((res)=>{
      if(res["message"]=="product added"){
        alert("product added to your cart");
     this.productcart=res["cart"]
     this.productcart.totalprice=this.ul.totalprice;
     console.log("cart price total",this.productcart.totalprice)
        }

        })
      }
        
  cartusername:any;
  // userprice:any;
  ngOnInit() {
    
    this.ar.paramMap.subscribe((param)=>{
      this.username=param.get("username");
      console.log(this.username);
      this.ul.username=this.username;
      this.pl.username=this.username;
      })
      
    this.pl.getuserproducts().subscribe((res)=>{
      
           this.productlist=res["message"];
           
      console.log(this.productlist);
    })
   
    
    
  
}
  
  sendveg()
  {
  for(let v of this.productlist)
  {
    console.log(v.category)
    if(v.category=="vegetables")
    {
      this.vegetables.push(v)
      console.log(this.vegetables)
      this.statusveg=true;
      this.productliststatus=false;
      this.statusleaf=false;
      this.statusfruits=false;
    }
  }
}  
sendleafveggies()
{
  for(let v of this.productlist)
  {
    console.log(v.category)
    if(v.category=="leafy veggies")
    {
      this.leafyvegetables.push(v)
      console.log(this.leafyvegetables)
      this.statusleaf=true;
      this.productliststatus=false;
      this.statusfruits=false;
      this.statusveg=false;
    }
  }
}
sendfruits()
{
  for(let v of this.productlist)
  {
    console.log(v.category)
    if(v.category=="fruits")
    {
      this.fruits.push(v)
      console.log(this.fruits)
      this.statusfruits=true;
      this.productliststatus=false;
      this.statusveg=false;
      this.statusleaf=false;

    }
  }
}

cart()
{
  this.router.navigate(['./shoppingcart'])
  
}
}

 
    

  
  



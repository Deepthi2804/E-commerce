import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';
import { ProductlistService } from 'src/app/productlist.service';
import { stringify } from 'querystring';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css']
})
export class ShoppingcartComponent implements OnInit {
  cartdata:any=[];
  cartname:string;
  userquantity:any;
  totalprice:number;
  cartprice:number;
  deletedproduct:string;
  cartquantity:number;
cartstatus:boolean=false;
  
  constructor(private ul:UserserviceService,private pl:ProductlistService,private router:Router) { }

 
  ngOnInit() {
    this.totalprice=0;
  this.cartprice=0;
 
  
    //getting products into cart
    this.ul.getcartdata().subscribe((res)=>{
      res.username=this.ul.username;
      console.log("response",res);
          if(res.username==this.ul.username)
      {
        this.cartdata=res["message"];
        this.ul.cartdata=this.cartdata;
       console.log("cart data is",this.cartdata);
      //  this.ngOnInit();
       for(let a of this.cartdata)
       {
         this.totalprice=this.totalprice+a.cartprice;
         this.ul.totalprice=this.totalprice;
          this.deletedproduct=this.cartdata.name;
       }
      //  this.ul.totalprice=this.totalprice;
      //  console.log("total price",this.ul.totalprice);
      //  this.cartdata.totalprice=this.ul.totalprice;
      //  console.log("total price",this.cartdata.totalprice);
      }
      
      //  if(res["message"]=="no product"){
      //   alert("No product in your Basket");
        
      // }
    })
    // if(this.cartdata.length!==0)
    // {
    //   this.cartstatus=true;
    // }
  }
  decrease(v)
{
  if(v.userquantity==0){
    v.quantity=v.quantity+0.5;
   
    //edit the product data
    this.ul.updatecart(v).subscribe((res)=>{
      res["message"];
      this.ngOnInit();
   
    this.ul.deleteitem(v).subscribe((res)=>{
      if(res["message"]=="product deleted")
      alert(res["message"]);
      this.ngOnInit();
    })
  })
  }
  else{
    v.quantity=v.quantity+0.5;
    
    this.pl.edit(v).subscribe((res)=>{
      console.log("product edited",res["message"])
      v.userquantity=v.userquantity-0.5;
      v.cartprice=v.cartprice-v.price;
      this.ul.updatecart(v).subscribe((res)=>{
        res["message"];
        this.ngOnInit();
      })
    })
  }

}
increase(v)
{
if(v.userquantity>=v.quantity){
  alert("limited stock");
  this.ngOnInit();
}
else{
  v.quantity=v.quantity-0.5;
  
  this.pl.edit(v).subscribe((res)=>{
    
    console.log("product edited",res["message"])
    v.userquantity=v.userquantity+0.5;
  v.cartprice=v.cartprice+v.price;
      // this.ngOnInit();
 
     this.ul.updatecart(v).subscribe((res)=>{
      res["message"];
      this.ngOnInit();
   
  })
})
}
}
username:any;

deletecart(){
  this.username=this.ul.username;
this.ul.deletecart(this.username).subscribe((res)=>{
    if(res["message"]=="cart deleted"){
      alert("cart items deleted")
      this.ngOnInit();
    // res["result"]
     }
  
})
}

checkout()
{
  this.cartdata.totalprice=this.totalprice;
  console.log("obj totalprice",this.cartdata.totalprice)
this.router.navigate(['/orders'])
this.cartdata.totalprice=this.totalprice;
}
order(obj)
{
  obj.username=this.ul.username;
this.ul.postorder(obj).subscribe((res)=>{
    if(res["message"]=="order placed"){
    alert("order placed");
    res["order"];
  }
})
}
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchproduct'
})
export class SearchproductPipe implements PipeTransform {

  transform(productlist:object[],searchterm:string):any {
    if(searchterm==undefined)
    {
      return productlist;
    }
    else{
      return productlist.filter(x=>x["name"].toLowerCase().indexOf(searchterm.toLowerCase())!==-1)
    }
  }


}

import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductlistService } from '../productlist.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
data:any=[]
  constructor(private ls:LoginService,private router:Router,private pl:ProductlistService) { }
  login(obj)
  {
    this.ls.doLogin(obj).subscribe((res)=>{
      
      if(res["message"]=="invalid username")
      {
        alert("Invalid password");
      }
      else if(res["message"]=="invalid password")
      {
        alert("Invalid password");
      }
      else{
       alert(res["message"]);
       this.data=res.userobj;
       console.log("data is",this.data)
       localStorage.setItem("token",res["token"])
          this.ls.userstatus=true;
          //get username
          this.ls.username=res["username"];
         

         if(this.data.role=="user"){
          this.router.navigate(['../userdashboard',this.data.username]);
          
         }
         if(this.data.role=="admin"){
          this.router.navigate(['../admindashboard']);
         }
         
       
          }
          


    })

   }
  //  doLogout()
  //  {
  //    localStorage.removeItem("token");
  //    this.ls.userstatus=false;
  //  }
 
   ngOnInit(): void {
    
     setTimeout(()=>{
       this.ls.doLogout(),0})
     }
// gotoRegister(data){
//   this.router.navigate(['/register'])

// }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { HomeproductsComponent } from './homeproducts/homeproducts.component';


const routes: Routes = [{path:"home",component:HomeComponent},{path:"login",component:LoginComponent}
                        ,{path:"register",component:RegisterComponent},
                        {path:"forgotpassword",component:ForgotpasswordComponent},{path:"homeproducts",component:HomeproductsComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

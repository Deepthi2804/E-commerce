//import express module
const expr=require("express");
//get express object
const app=expr();
//import path 
const path=require("path");
app.use(expr.static(path.join(__dirname,"/dist/e-commerce")))
//importing apis
 const userapi=require('./apis/userapi');
 const adminapi=require('./apis/adminapi');
 const cartapi=require('./apis/cartapi');
 const orderapi=require('./apis/orderapi');
 const shippingapi=require('./apis/shippingapi');
// //forwarding req object
app.use('/user',userapi);
app.use('/admin',adminapi);
app.use('/cart',cartapi);
app.use('/order',orderapi);
app.use('/ship',shippingapi);




//assign port number
const port=3500;
app.listen(port,()=>{
    console.log(`server is running on port ${port}...`);
})

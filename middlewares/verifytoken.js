const jwt=require("jsonwebtoken")
var verifyToken=(req,res,next)=>{
//extract token from header of req object
console.log("req header is",req.headers.authorization)
var tokenWithBearer=req.headers.authorization;
if(tokenWithBearer)
{
//get token by removing first 7 characters from tokenWithBearer
var signedtoken=tokenWithBearer.slice(7,tokenWithBearer.length);
console.log("token after removing is",signedtoken);
jwt.verify(signedtoken,'aabbcc',(err,decodedToken)=>{
    if(err)
    {
        res.send({message:"pls relogin"});
    }
    else{
        //forward the req obj to req handler
        next();
    }
})

}
else{
    res.send({message:"plz login"})
}
}
//export verifytoken function
module.exports=verifyToken;
//import express module
const expr=require("express");
const shippingapi=expr.Router();
const mc=require("mongodb").MongoClient;
var dbo;
//get url of database
const dburl="mongodb://Deepthi:Deepthi@cluster0-shard-00-00-v36bw.mongodb.net:27017,cluster0-shard-00-01-v36bw.mongodb.net:27017,cluster0-shard-00-02-v36bw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
//connect to the database using above url
mc.connect(dburl,{useNewUrlParser:true,useUnifiedTopology:true},(err,client)=>{
    if(err)
    {
        console.log("error occured in",err);
    }
    else
    {
        //get database object
        dbo=client.db("deepthi");
        console.log("connected to db...")
    }
})
shippingapi.use(expr.json());
shippingapi.post('/postaddress',(req,res)=>{
   
    console.log("req obj is",req.body);
       
            dbo.collection("shippingcollection").insertOne(req.body,(err,address)=>{
                if(err)
                {
                    console.log("error occured",err);
                }
                else{
                    res.send({message:"added to cart"})
                }
            })
        
    })
    
//total orders
shippingapi.get('/totalorders',(req,res)=>{
    dbo.collection("shippingcollection").find().toArray((err,totalorders)=>{
        if(err)
        {
            console.log("error in totalordes",err);
        }
        else if(totalorders.length==0)
        {
            res.send({message:"no orders"})
        }
        else{
            res.send({message:"totalorders",totalorders:totalorders})
        }
    })
})
shippingapi.get('/userorders/:date',(req,res)=>{
    console.log("body is",req.body);
    dbo.collection("shippingcollection").findOne({date:req.params.date},(err,userorders)=>{
        if(err)
        {
            console.log("error in userorders",err);
        }
        else if(userorders==null)
        {
            res.send({message:"no orders"});
        }
        else{
            res.send({message:"your orders",userorders:userorders});
        }
})
})
//export module
module.exports=shippingapi;
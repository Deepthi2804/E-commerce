//import express module
const expr=require("express");
const adminapi=expr.Router();
const mc=require("mongodb").MongoClient;
var dbo;
//get url of database
const dburl="mongodb://Deepthi:Deepthi@cluster0-shard-00-00-v36bw.mongodb.net:27017,cluster0-shard-00-01-v36bw.mongodb.net:27017,cluster0-shard-00-02-v36bw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
//connect to the database using above url
mc.connect(dburl,{useNewUrlParser:true,useUnifiedTopology:true},(err,client)=>{
    if(err)
    {
        console.log("error occured in",err);
    }
    else
    {
        //get database object
        dbo=client.db("deepthi");
        console.log("connected to db...")
    }
})
adminapi.use(expr.json());
//post req handler
adminapi.post('/product',(req,res)=>{
    console.log(req.body);
    dbo.collection("admincollection").findOne({name:req.body.name},(err,product)=>{
        if(err)
        {
            console.log("error occured in read",err);
        }
        else if(product==null)
        {
            // res.send({message:"no product"})
            dbo.collection("admincollection").insertOne(req.body,(err,productlist)=>{
                if(err)
                {
                    res.send({message:err.message})
                }
                else{
                    res.send({message:productlist});
                }
            })
           
        }
        else{
           res.send({message:"product existed"})
        }
    })
})
//get req handler
adminapi.get('/productdata/:name',(req,res)=>{
    dbo.collection("admincollection").findOne({name:req.params.name},(err,product)=>{
        if(err)
        {
            res.send({message:err.message})
        }
        else if(product==null)
        {
            res.send({message:"no product"})
        }
        else{
            res.send({message:product})
        }
    })

})
//user product get req handler
adminapi.get('/userproductdata',(req,res)=>{
    dbo.collection("admincollection").find().toArray((err,product)=>{
        if(err)
        {
            res.send({message:err.message})
        }
        else if(product.length==0)
        {
            res.send({message:"no product"})
        }
        else{
            res.send({message:product})
        }
    })

})

//rea handler to get products
adminapi.get('/productdata',(req,res)=>{
    console.log(req.body);
    dbo.collection("admincollection").find().toArray((err,product)=>{
        if(err)
        {
            res.send({message:err.message})
        }
        else if(product.length==0)
        {
            res.send({message:"no product"})
        }
        else{
            res.send({message:product})
        }
    })

})
//editdata product
adminapi.get('/editdataproduct/:name',(req,res)=>{
    console.log(req.params)
    dbo.collection("admincollection").findOne({name:req.params.name},(err,product)=>{
        if(err)
        {
            console.log(err)
        }
        else if(product==null)
        {
            res.send({message:"no product"})
        }
        else{
            res.send({message:product})
        }
    })

})
//update req handler
adminapi.put('/updateproduct',(req,res)=>{
    dbo.collection("admincollection").findOne({name:req.body.name},(err,product)=>{
        if(err)
        {
            res.send({message:err.message})
        }
        else if(product==null)
        {
            res.send({message:"no product"})
        }
        else{
            dbo.collection("admincollection").updateOne({name:req.body.name},{$set:{id:req.body.id,quantity:req.body.quantity,cartprice:req.body.cartprice,
                            price:req.body.price,category:req.body.category,url:req.body.url,userquantity:req.body.userquantity}},(err,result)=>{
                                if(err)
                                {
                                    res.send({message:err.message})
                                }
                                else
                                {
                                    res.send({message:"product updated"})
                                }
                            })
        }
    })
})
//req handler to delte product
adminapi.delete('/deleteproduct/:name',(req,res)=>{
    console.log(req.params)
    dbo.collection("admincollection").findOne({name:req.params.name},(err,product)=>{
        if(err)
        {
            //res.send({message:err.message})
            console.log(err)
        }
        else if(product==null)
        {
            res.send({message:"no product"})
        }
        else{
            dbo.collection("admincollection").deleteOne({name:req.params.name},(err,deletedproduct)=>{
                if(err)
                {
                    //res.send({message:err.message})
                    console.log(err)
                }
                else{
                    res.send({message:"product deleted"})
                }
            })
        }
    })
})
//export adminapi
module.exports=adminapi;
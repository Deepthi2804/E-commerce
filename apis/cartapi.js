//import express module
const expr=require("express");
const cartapi=expr.Router();
const mc=require("mongodb").MongoClient;
var dbo;
//get url of database
const dburl="mongodb://Deepthi:Deepthi@cluster0-shard-00-00-v36bw.mongodb.net:27017,cluster0-shard-00-01-v36bw.mongodb.net:27017,cluster0-shard-00-02-v36bw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
//connect to the database using above url
mc.connect(dburl,{useNewUrlParser:true,useUnifiedTopology:true},(err,client)=>{
    if(err)
    {
        console.log("error occured in",err);
    }
    else
    {
        //get database object
        dbo=client.db("deepthi");
        console.log("connected to db...")
    }
})
cartapi.use(expr.json());
//to store cart data
cartapi.post('/cartdata',(req,res)=>{
    dbo.collection("cartcollection").insertOne(req.body,(err,cartobj)=>{
        if(err)
        {
            console.log("error occured in cart save",err)
        }
        else{
            res.send({message:"product added",cartobj:cartobj})
        }
    })
})
// cartapi.post('/cartdata/:name/:username',(req,res)=>{
//     dbo.collection("cartcollection").findOne({$or:[{name:req.params.name},{username:req.params.username}]},(err,booleanobj)=>{
//         if(err)
//         {
//             res.send({message:err.message})
//         }
//         else if(booleanobj)
//         {
//             dbo.collection("cartcollection").insertOne(req.body,(err,cartobj)=>{
//                 if(err)
//                 {
//                     res.send({message:err.message})
//                 }
//                 else{
//                     res.send({message:"product added",cartobj:cartobj})
//                 }
//             })
//         }
//         else{
//             res.send({message:"no data"});
//         }
//     })
// })
//get cart data 
cartapi.get('/getcartdata/:username',(req,res)=>{
    console.log("body is",req.body);
    dbo.collection("cartcollection").find({username:req.params.username}).toArray((err,cartdata)=>{
        if(err)
        {
            console.log("error occured",err);
        }
        else if(cartdata.length==0){
            res.send({message:"no product"})
        }
        else{
            res.send({message:cartdata});
            console.log("cart from db is",res[cartdata]);
        }

    })
})
cartapi.delete('/deleteitem/:name',(req,res)=>{
    dbo.collection("cartcollection").findOne({name:req.params.name},(err,deletedproduct)=>{
        if(err)
        {
            console.log("error in deletion",err)
        }
        else if(deletedproduct==null){
            res.send({message:"no product to delete"})
        }
        else{
            dbo.collection("cartcollection").deleteOne({name:req.params.name},(err,result)=>{
                if(err)
                {
                    console.log("error occured in delete",err);
                }
                else{
                    res.send({message:"product deleted"});
                }
            })
        }
    })
})
//update cart details4
cartapi.put('/updatecart',(req,res)=>{
    dbo.collection("cartcollection").updateOne({name:req.body.name},{$set:{quantity:req.body.quantity,
        cartprice:req.body.cartprice,price:req.body.price,userquantity:req.body.userquantity}},(err,result)=>{
            if(err){
                console.log("error in updation",err);
            }
            else{
                res.send({message:result})
            }
        })
})
cartapi.delete('/deletecart/:username',(req,res)=>{
    dbo.collection("cartcollection").find({username:req.params.username}).toArray((err,deletedobj)=>{
        if(err){
            console.log("error in deletecart",err);
        }
        else if(deletedobj==0){
            res.send({message:"no product"})
        }
        else{
            dbo.collection("cartcollection").deleteMany({username:req.params.username},(err,result)=>{
                if(err)
                {
                    res.send({message:"error in clearcart"})
                }
                else{
                    res.send({message:"cart deleted",result:result});
                }
            })
            
        }

    })
})
// cartapi.post('/postaddress',(req,res)=>{
//     console.log("req obj is",req.body);
       
//             dbo.collection("cartcollection").insertOne(req.body,(err,address)=>{
//                 if(err)
//                 {
//                     console.log("error occured",err);
//                 }
//                 else{
//                     res.send({message:"added to cart"})
//                 }
//             })
        
//     })

//to add address
// cartapi.put('/address/:username',(req,res)=>{
//     console.log("req object is",req.body);
//     dbo.collection("cartcollection").find({username:req.params.username}).toArray((err,addr)=>{
//         if(err)
//         {
//             console.log("error in address",err);
//         }
//         else if(addr.length==0)
//         {
//             res.send({message:"no username"})
//         }
//         else{
//             dbo.collection("cartcollection").updateMany({username:req.body.username},{$set:{address:req.body}},(err,address)=>{
//                 if(err)
//                 {
//                     console.log("error occured in address",err);
//                 }
//                 else{
//                     res.send({message:"address added",address:address})
//                 }

//             })
//         }
//     })
// })

//export cartapi
module.exports=cartapi;
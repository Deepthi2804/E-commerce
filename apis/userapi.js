//import express module
const expr=require("express");
const userapi=expr.Router();
//import verify token
const verifytoken=require("../middlewares/verifytoken")
const mc=require("mongodb").MongoClient;
var dbo;
//get url of database
const dburl="mongodb://Deepthi:Deepthi@cluster0-shard-00-00-v36bw.mongodb.net:27017,cluster0-shard-00-01-v36bw.mongodb.net:27017,cluster0-shard-00-02-v36bw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
//connect to the database using above url
mc.connect(dburl,{useNewUrlParser:true,useUnifiedTopology:true},(err,client)=>{
    if(err)
    {
        console.log("error occured in",err);
    }
    else
    {
        //get database object
        dbo=client.db("deepthi");
        console.log("connected to db...")
    }
})
//define get req handler to read data in database
userapi.get('/readusers',(req,res)=>{
    dbo.collection("usercollection").find().toArray((err,userArray)=>{
        if(err)
        {
            console.log("error occured in read",err);
        }
        else if(userArray.length==0)
        {
            res.send({message:"no user existed"})
        }
        else{
            res.send({message:userArray});
        }
    })
})

//handling get req to read one username from database what ever user entered by his own

userapi.get('/readuser/:username/userdashboard',(req,verifytoken,res)=>{
    //read one user from database
    dbo.collection("usercollection").findOne({username:req.params.username},(err,userobj)=>
    {
        if(err)
        {
            console.log("error occured in readone",err)
        }
        else if(userobj==null)
        {
            res.send({message:"no user"});
        }
        else{
            var otp=Math.floor(Math.random()*99999)
            res.send({message:userobj,otp:otp});
        }
    })

})
//post method
userapi.use(expr.json());
//post req handler to create a user
userapi.post('/createuser',(req,res)=>{
    //get data sent by client from body property of request object
    console.log("data is",req.body);
    const bcrypt=require('bcrypt');
            let pwd=bcrypt.hashSync(req.body.password,6);
            //repalce plain text pwd with hashed pwd
            req.body.password=pwd;
            console.log(req.body); 
    //save to user collection
    dbo.collection("usercollection").insertOne(req.body,(err,result)=>
    {
        if(err)
        {
            console.log("error occured in post",err);
        }
        else{
            
            res.send({message:`user ${req.body.username} created succesfully`});
        }

    })
})
//check for user existence in database with username
//userapi.use(expr.json());

userapi.post('/register',(req,res)=>{
    
         dbo.collection("usercollection").findOne({username:req.body.username},(err,userobj)=>{
            
        if(err)
        {
            console.log("error occured in post",err);
        }
        else if(userobj==null){
            let hashedpw=bcrypt.hashSync(req.body.password,6);
                        req.body.password=hashedpw;
            
            dbo.collection("usercollection").insertOne(req.body,(err,success)=>{
                if(err)
                {
                    console.log("error occured in creating",err);
                }
                else{
                    
                    res.send({message:"user created",userobj:userobj})
                }
                
            })
        }
        else{
            
            res.send({message:"user already existed"})
        }
    
    })
})


//post method to validate login credentials
const jwt=require("jsonwebtoken")
const bcrypt=require("bcrypt");
userapi.post('/login',(req,res)=>{
    console.log(req.body);
    //user credentials
    //compare username
   
    dbo.collection("usercollection").findOne({username:req.body.username},(err,userobj)=>{
        if(err)
        {
            console.log("error occured in compare",err);
        }
        else if(userobj==null)
        {
            res.send({message:"invalid username"});
        }
        //username matched,compare with password
        else{
                           
            bcrypt.compare(req.body.password,userobj.password,(err,isMatched)=>{
                                
                if(err)
                {
                    console.log("error in compare",err);
                }
                if(isMatched==false)
                {
                    res.send({message:"invalid password"});
                }
                //if pw also matched create,sign and send token to client
                else
                {
                   
                    jwt.sign({username:userobj.username},'aabbcc',{expiresIn:58},(err,signedtoken)=>{
                        
                        if(err){
                            console.log("error in sign in",err);
                        }
                        else{
                            res.send({message:"logged in successfully",token:signedtoken,userobj:userobj});
                        }
                    })
                }

            })
        }

    })
})

//update operation
userapi.put('/updateuser',(req,res)=>{
    //data sent by client is in "body" property of "request" object
    console.log(req.body);
    dbo.collection("usercollection").findOne({username:req.body.username},(err,userobj)=>{
        if(err)
        {
            console.log("error occured in search",err);
        }
        else if(userobj==null)
        {
            res.send({message:`user is ${req.body.username} not existed`})
        }
        else{
            //modify the object
            dbo.collection("usercollection").update({username:req.body.username},
                {$set:{email:req.body.email,age:req.body.age}},(err,result)=>{
                    if(err)
                    {
                        console.log("error occured in updation",err);
                    }
                    else{
                        res.send({message:`user ${req.body.username} is updated`});
                    }

                })
        }
        
    })
})
//delete username by passing a parameter 
userapi.delete('/deleteuser/:username',(req,res)=>
{
    dbo.collection("usercollection").findOne({username:req.params.username},(err,userobj)=>{
        if(err)
        {
            console.log("error occured in search",err);
        }
        else if(userobj==null)
        {
            res.send({message:`user ${req.params.username} is not existed`})
        }
        else
        {
            //delete the object
            dbo.collection("usercollection").deleteOne({username:req.params.username},(err,result)=>{
                if(err)
                {
                    console.log("error occured in delete",err);
                }
                else{
                    res.send({message:`user ${req.params.username} is deleted`});
                }

            })
        }
    })
})

//get the details by email id
userapi.get('/readuser/:email',(req,res)=>{
    //read one user from database
    dbo.collection("usercollection").findOne({email:req.params.email},(err,userobj)=>
    {
        if(err)
        {
            console.log("error occured in readone",err)
        }
        else if(userobj==null)
        {
            res.send({message:"no email"});
        }
        else{
            //Math.random()*99999
            var otp=Math.floor(Math.random()*99999)
            console.log(otp);
            res.send({message:userobj,otp:otp});
        }
    })

})
//to get username from url
// userapi.get('/login/:username/userdashboard',(req,res)=>{
//     console.log("username is",req.params)
//     //read userobj and send it as response
//     dbo.collection("usercollection").findOne({username:req.params.username},(err,userobj)=>{
//         if(err)
//         {
//             console.log("error occured in read",err);
//         }
//         else{
//             res.send({message:userobj});
//         }

//     })
// })


// //req handlers
// userapi.get('/readuser',(req,res)=>{
//     res.send({message:'read request handler is working'});
// })
// userapi.get('/saveuser',(req,res)=>{
//     res.send({message:'save request handler is working'});
// })
// userapi.get('/updateuser',(req,res)=>{
//     res.send({message:'update request handler is working'});
// })
// userapi.get('/deleteuser',(req,res)=>{
//     res.send({message:'delete request handler is working'});
// })




//export userapi module
module.exports=userapi;
//import express module
const expr=require("express");
const orderapi=expr.Router();
const mc=require("mongodb").MongoClient;
var dbo;
//get url of database
const dburl="mongodb://Deepthi:Deepthi@cluster0-shard-00-00-v36bw.mongodb.net:27017,cluster0-shard-00-01-v36bw.mongodb.net:27017,cluster0-shard-00-02-v36bw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
//connect to the database using above url
mc.connect(dburl,{useNewUrlParser:true,useUnifiedTopology:true},(err,client)=>{
    if(err)
    {
        console.log("error occured in",err);
    }
    else
    {
        //get database object
        dbo=client.db("deepthi");
        console.log("connected to orderapi & db...")
    }
})
orderapi.use(expr.json());
//post req for orderdata
orderapi.post('/orderdata',(req,res)=>{
    dbo.collection("orderscollection").insertOne(req.body,(err,address)=>{
        if(err)
        {
            res.send({message:err.message});
        }
        else{
            res.send({message:"address added",address:address});
        }
    })
    })
//get address
orderapi.get('/getaddress/:username',(req,res,next)=>{
    console.log(req.body);
    dbo.collection("orderscollection").find({username:req.params.username}).toArray((err,addr)=>{
        if(err)
        {
            res.send({message:err.message})
        }
        else if(addr.length==0){
            res.send({message:"no address"})
        }
        else{
            res.send({message:"address",addr:addr})
        }
    })
})
//to delete address
orderapi.delete('/deleteaddress/:username',(req,res)=>{
    dbo.collection("orderscollection").findOne({username:req.params.username},(err,deletedobj)=>{
        if(err)
        {
            connsole.log("error occured",err);
        }
        else if(deletedobj==null)
        {
            res.send({message:"no address"})
        }
        else{
            dbo.collection("orderscollection").deleteOne({username:req.params.username},(err,deletedaddr)=>{
                if(err)
                {
                    console.log("error in address deletion",err);
                }
                else{
                    res.send({message:"address deleted"})
                }
            })
        }
    })
    
})

//export a module
module.exports=orderapi;